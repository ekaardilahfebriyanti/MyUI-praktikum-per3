package com.ekaardilahfebriyanti.myui;

import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class ImageViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_view);
    }
    public void loadGithub(View v){
        String uri = "@drawable/github"; //where myresource (without the extension) is the file
        int imageResource = getResources().getIdentifier(uri, null,getPackageName());
        ImageView imageview = (ImageView) findViewById(R.id.iV);
        Drawable res = getResources().getDrawable(imageResource);
        imageview.setImageDrawable(res);
    }
    public void loadCodepen(View v){
        String uri = "@drawable/codepen";
        int imageResource = getResources().getIdentifier(uri,null,getPackageName());
        ImageView imageview = (ImageView) findViewById(R.id.iV);
        Drawable res = getResources().getDrawable(imageResource);
        imageview.setImageDrawable(res);
    }
}
